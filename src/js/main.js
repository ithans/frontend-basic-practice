import Person from './Person';
import FetchData from './FetchData';
import renderNameAndAge from './RenderNameAndAge';
import renderEducation from './RenderEducation';

const URL = 'http://localhost:3000/person';
FetchData(URL)
  .then(result => {
    const person = new Person(
      result.name,
      result.age,
      result.description,
      result.educations
    );
    renderNameAndAge(person);
    renderEducation(person);
  })
  .catch(error => {
    document.writeln(JSON.stringify(error));
  });
