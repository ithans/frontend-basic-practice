import $ from 'jquery';

function renderNameAndAge(person) {
  $('.about-section span')
    .empty()
    .append(person.description);
  $('name')
    .empty()
    .append(person.name);
  $('age')
    .empty()
    .append(person.age);
}

export default renderNameAndAge;
