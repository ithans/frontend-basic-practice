import $ from 'jquery';

function renderEducation(person) {
  const mainArticle = $('.main-article');
  person.educations.forEach(education => {
    let section = $('<section></section>');
    let span = $(`<span>${education.year}</span>`);
    let div = $('<div></div>');
    let strong = $(`<strong>${education.title}</strong>`);
    let p = $(`<p>${education.description}</p>`);
    div.append(strong);
    div.append($('<br>'));
    div.append(p);
    section.append(span);
    section.append(div);
    section.appendTo(mainArticle);
  });
}

export default renderEducation;
